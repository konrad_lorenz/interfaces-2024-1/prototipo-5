# Taller: Desarrollo de una Aplicación de Detección de Emociones

## Guia implementacion:
![Arquitectura de Referencia](![Alt text](guia.png))

## Objetivos
- Crear un microservicio API con FastAPI que recibe imágenes y utiliza un modelo de inteligencia artificial para detectar emociones.
- Desarrollar una aplicación web que permita a los usuarios cargar imágenes y visualizar los resultados de la predicción de emociones.
- Integrar la aplicación web con el microservicio API.

## Requisitos Previos
- Repositorio Git (github o gitlab)
- Conocimientos básicos de Python.
- Conocimientos en desarrollo web (HTML, CSS, JS).
- Entorno de desarrollo con Python y pip instalados.
- Acceso al código fuente de la red neuronal entrenada para reconocer emociones (disponible en un Colab).

## Materiales
- Videos educativos sobre FastAPI y desarrollo web.
- Acceso a clases grabadas sobre APIs y consumo de servicios web.
- Notebook de Google Colab donde se entrenó la red neuronal.
- Documentación de FastAPI, Bootstrap y jQuery.

## Parte 1: Creación de la API con FastAPI

### Configuración del Entorno:
- Crear un entorno virtual Python y activarlo.
- Instalar FastAPI y Uvicorn, que servirá como nuestro servidor ASGI.
- Instalar cualquier biblioteca adicional requerida para ejecutar el modelo de IA.

### Desarrollo de la API:
- Iniciar un nuevo proyecto FastAPI.
- Crear un endpoint POST que acepte imágenes como entrada.
- Cargar el modelo de IA entrenado y prepararlo para la inferencia.
- Realizar la predicción de emociones en la imagen recibida.
- Devolver los resultados de la predicción como respuesta del endpoint.

## Parte 2: Desarrollo de la Aplicación Web

### Estructura de la Aplicación Web:
- Crear un nuevo proyecto con archivos base de HTML, CSS y JavaScript.
- Utilizar Bootstrap para diseñar una interfaz de usuario atractiva y responsiva.
- Añadir un formulario para subir imágenes y un botón para enviar la predicción.

### Interacción con la API:
- Utilizar jQuery para manejar la acción de cargar la imagen y realizar la solicitud POST al microservicio API.
- Mostrar los resultados de la predicción en la interfaz de usuario.

## Parte 3: Integración y Pruebas

### Integración Completa:
- Asegurarse de que la aplicación web y el microservicio API están correctamente conectados.
- Verificar que el flujo completo (carga de imagen, predicción y visualización de resultados) funciona sin errores.

### Pruebas y Depuración:
- Probar la aplicación con varias imágenes para asegurar que la detección de emociones es precisa.
- Depurar cualquier problema que surja durante las pruebas.

## Entrega
- Los participantes deberán subir su código a un repositorio de git.
- Deben incluir un `README.md` con instrucciones sobre cómo configurar y ejecutar la aplicación.
- Realizar una presentación (maximo 3 min) de su aplicación, demostrando su uso y las emociones detectadas.
- Reporte latex con las evidencias de pruebas de la parte 1 y la parte 2 (ver imagen)

## Evaluación
- La funcionalidad del microservicio API.
- La usabilidad y diseño de la interfaz de usuario.
- La correcta integración entre la aplicación web y el API.
- La calidad del código y la implementación de prácticas de programación adecuadas.

Este taller es un proyecto práctico que permite a los participantes aplicar sus conocimientos en un escenario de la vida real, fomentando el aprendizaje a través de la experiencia directa en el desarrollo de aplicaciones web y servicios de backend.


## Videos Complementarios de apoyo

- 
-